#! /bin/sh
set -e
cd "$(dirname "$0")"

md5cmd=md5sum
if command -v md5 >/dev/null; then
    md5cmd=md5
fi

tarcmd=tar
# On OpenBSD we need GNU tar(1) for the right `-O`.
if [ "$(uname)" = OpenBSD ]; then
    tarcmd=gtar
fi

# Filenames.
./dectar test/* \
| "$tarcmd" tf - \
| tr '\n' ' ' \
| grep -Eq "^test/bar test/baz test/default test/foo test/quux test/snork $" \
|| exit 1

# Text file contents.
./dectar test/* \
| "$tarcmd" -xOf - \
| tr '\n' ' ' \
| grep -Eq "^Bar. Baz. Don't mind me. Foo. Quux. Snork. $" \
|| exit 2

# Binary file contents.
./dectar test/.bin \
| "$tarcmd" -xOf - \
| "$md5cmd" \
| grep -Eq "^8112240dbac53e8224388930f612e026" \
|| exit 3
