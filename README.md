# dectar

Take a list of compressed files and output a tar archive of their decompressed contents.  With this script you can reduce the size of old regular backups (for example, `.sql.bz2` database dumps) by decompressing them individually and compressing them together.


## Installation

1. Install Python 3 and the command line utilities for bzip2, Gzip, lz4, xz, and zstd. Optionally, install md5sum(1) to run the tests, if you don't have md5(1) in the base system (DragonFly/Free/Net/OpenBSD). On Debian/Ubuntu: `sudo apt install python3 bzip2 gzip lz4 xz-utils zstd`
2. Clone the repository with `git clone https://gitlab.com/dbohdan/dectar` or download the file `dectar`.


## Usage

```sh
./dectar file1 ... fileN > files.tar
```

### Example

```sh
$ ls test/
bar.bz2  baz.xz  default  foo.gz  qux.zst
$ ./dectar test/* | gzip -9 | tar tzf -
test/bar
test/baz
test/default
test/foo
test/qux
```


## License

MIT.
